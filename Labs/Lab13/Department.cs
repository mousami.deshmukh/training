﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab13
{
    class Department
    {
        public int id;
        public string name, location;
        public Department(int d_id,string d_name,string d_loc)
        {
            id = d_id;
            name = d_name;
            location = d_loc;
        }

        public override string ToString()
        {
            return "Name is " + name + ", ID is " + id+ " Loc: "+location;
        }
    }
}
