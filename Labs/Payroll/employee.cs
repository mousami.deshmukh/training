﻿using System;

namespace Payroll
{
    public class employee

    {
        private int e_id;
        private string e_na;
        private string e_de;
        private int e_sa;
        private string e_da;

        public employee(int id, string name, string designation, int salary, string date)
        {
            e_id = id;
            e_na = name;
            e_de = designation;
            e_sa = salary;
            e_da = date;
        }

        public void print()
        {
            Console.WriteLine("ID : " +e_id+ "\n Name : "+e_na+ "\n Designaiton : " +e_de+ "\n Salary : " +e_sa+ "\n Date of joining :" +e_da);
        }
    }
}
