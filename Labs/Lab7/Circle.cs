﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Circle:Shape
    {
        private double radius,area;
       public  Circle(double radius_c)
        {
            radius = radius_c;
        }
        public override void  Area()
        {
             area = 3.14 * radius * radius;
            
        }

        public void printArea()
        {
            Console.WriteLine("the area of circle is :"+area);
        }

    }
}
