﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2b
{
    class Employee
    {
        int eid;
        string ename;
        string edes;
        double esal,ehra,epf,gs,netsal;
        int dept_id;
  
        public Employee(int e_id,string e_na,string e_de,double e_sa,int d_id )
        {
            eid = e_id;
            ename = e_na;
            edes = e_de;
            esal = e_sa;
            ehra=(8 * e_sa)/100;
            epf = (12 * e_sa) / 100;
            gs =e_sa+ehra+1000;
            netsal =gs-(10+epf);
            dept_id = d_id;

        }

        public void print()
        {
            Console.WriteLine("Employee id: " +eid+  "\n Employee name : " +ename+ "\n Designation : " +edes+ "\n Base salary: " +esal+ "\n HRA : " +ehra+ "\n PF : " +epf+ "\n GROSS : " +gs+ "\n NET : " +netsal+ "\n Dept ID : " +dept_id);
        }
    }
}
