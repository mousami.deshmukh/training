﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Date dt1 = new Date();
            Date dt2 = new Date { mDay =Convert.ToInt32( args[0]), mMonth =Convert.ToInt32( args[1]), mYear = Convert.ToInt32(args[2] ) };
            dt1.Printdate();
            dt2.Printdate();

        }
    }
}
