﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array_int = new int[5] { 12,10,15,30,89};
            string[] array_str = new string[5] { "RED","BLUE","GREEN","YELLOW","ORANGE"};
            int[] array_copy = new int[7];

            System.Array.Copy(array_int,array_copy,5);
            Console.WriteLine("The integer array is: \n");
            PrintValues(array_int);
            Console.WriteLine("The copied integer array is: \n");
            PrintValues(array_copy);
            Console.WriteLine("Before Sorting : \n");
            PrintValues(array_str);
            Console.WriteLine("After sorting : \n");
            Array.Sort(array_str,StringComparer.InvariantCulture);
            PrintValues(array_str);
            Console.WriteLine("After reversing is: \n");
            Array.Reverse(array_str);
            PrintValues(array_str);

        }
        public static void PrintValues(String[] arr)
        {
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0);
                  i++)
            {
                Console.WriteLine("   [{0}] : {1}", i, arr[i]);
            }
            Console.WriteLine();
        }

        public static void PrintValues(int[] myArr)
        {
            foreach (int i in myArr)
            {
                Console.Write("\t{0}", i);
            }
            Console.WriteLine();
        }
    }
}

