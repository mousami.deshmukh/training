﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6b
{
    class Manager :Employee
    {
        private int petrol,food,hra,other,gross,net,ma_id;
        private string ma_name; 
        
        public Manager(int sal,string m_name,int m_id): base(sal,m_name,m_id)
        {
            hra = (8 * sal)/100;
            petrol = (8 * sal) / 100;
            food = (13 * sal) / 100;
            other = (3 * sal) / 100;
            gross = sal + petrol + food + other +hra ;
            net = gross - (1000 + 500);
            ma_id = m_id;
            ma_name = m_name;




        }

        public override string ToString()
        {
            return "\n Name :" +ma_name+ "\n ID is :" +ma_id+"\n HRA of Manager: " +hra  + "\n Petrol Allowance : " + petrol+ "\n Food Allowance :" +food+ "\n Other Allownace :" +other+ "\n Gross :" +gross+ "\n Net Salary :" +net;
        }
    }
}
