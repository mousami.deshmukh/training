﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6b
{
    class President:Employee
    {
        private int travel, p_hra, p_gross, p_net, p_id, p_kilo;
        private string p_name;
        public President(int e_base_sal,int kilo,string name,int id):base( e_base_sal,name,id)
        {
            p_hra = (8 * e_base_sal) / 100;
            travel = kilo * 8;   ///travel allowance
            p_gross = travel + p_hra; ///gross 
            p_net = p_gross - (1000 + 500); ///net
            p_name = name;
            p_id = id;
            p_kilo = kilo;

        }

        public override string ToString()
        {
            return "\n Name :" +p_name+ "\n ID :"+p_id+"\n Base salary :" +e_base+"\n Kilometres travelkled :" +p_kilo+"\n HRA : " + p_hra + "\n Travel Allowance : " + travel + "\n Gross :" + p_gross + "\n Net Salary :" + p_net;
        }
    }
}
