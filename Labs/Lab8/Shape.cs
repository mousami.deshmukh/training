﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Shape
    {
        private int rad;
        public Shape(int radius)
        {
            rad = radius;
        }
       public  void print()
        {
            Console.WriteLine("The area of circle is :" +(3.14*rad*rad));
        }
    }
}
