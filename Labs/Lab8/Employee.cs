﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Employee:IPrintable
    {
        private int  e_id;
        private string e_name, e_des,e_loc;
        public Employee(int id, string name, string des, string loc)
        {
            e_id = id;
            e_loc = loc;
            e_des = des;
            e_name = name;

        }

        public void  print()
        {
            Console.WriteLine("The ID :" +e_id+"\n Name :" +e_name+ "\n Location :" +e_loc+ "\n Designation :" +e_des);
        }
    }
}
