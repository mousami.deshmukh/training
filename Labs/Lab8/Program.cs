﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter employee id:");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter employee name");
            string name = Console.ReadLine();

            Console.WriteLine("Enter employee designation");
            string des = Console.ReadLine();

            Console.WriteLine("Enter employee location");
            string loc = Console.ReadLine();

            Employee one = new Employee(id,name,des,loc);
            one.print();

            Console.WriteLine("Enter radius of circle:");
            int radius = Convert.ToInt32(Console.ReadLine());
            Shape circle = new Shape(radius);
            circle.print();

            Console.WriteLine("Enter day:");
            int day = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter month:");
            int month = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter year:");
            int year = Convert.ToInt32(Console.ReadLine());

            Date date = new Date(year,month,day);
            date.print();
        }
    }
}
