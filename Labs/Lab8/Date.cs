﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Date:IPrintable
    {
        private int p_day, p_month, p_year;
        bool check;
        public Date(int year, int month, int day)
        {
            
            if (check == true)
            {
                p_day = day;
                p_year = year;
                p_month = month;
            }
            else
            {
                Console.WriteLine("wrong input...");
            }


           

        }
        public void print()
        {
            bool check = CheckValidDateTime(p_year, p_month, p_day);
            if (check == true)
            {


                Console.WriteLine("The date is :" + p_day + "/" + p_month + "/" + p_year);
            }
            else
            {
                Console.WriteLine("Wrong input...");
            }
        }

        bool CheckValidDateTime(int year, int month, int day)
        {
            bool check = false;
            if (year <= DateTime.MaxValue.Year && year >= DateTime.MinValue.Year)
            {
                if (month >= 1 && month <= 12)
                {
                    if (DateTime.DaysInMonth(year, month) >= day && day >= 1)
                        check = true;
                }
            }

            return check;

        }
       
    }
}
