﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CollectionApp
{
    class Program
    {

        private static int[] intValues = { 1,2,3,4,5,6};
        private static double[] doubleValues = { 3.8,9.0,2.9,3.4};
        private static int[] intValuesCopy; ///u can have claSS LEVEL VARIABLE WITHOUR BEING INITIALIZED
        static void Main(string[] args)
        {
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("initial array values:\n ");
            PrintArray();
            //sort double values sort is static methos of array class
            Array.Sort(doubleValues);

            //copy values from intValues to intVlauesCopy array
            Array.Copy(intValues,intValuesCopy,intValues.Length);
            Console.WriteLine("\n array vlaues after sotrr and copy ");
            PrintArray();

            int result = Array.BinarySearch(intValues,5);
            if(result>=0)
            {
                Console.WriteLine("5 found at element{0} in intValues",result);
            }
            else
            {
                Console.WriteLine("5 not found in int values !!!!");
            }

            //search 8783 in intValues

            int res2 = Array.BinarySearch(intValues,8783);
            if(res2>=0)
            {
                Console.WriteLine("8783 found at rlrmnt {0}",res2);
            }
            else
                {
                Console.WriteLine("8783 not found in int values !!!");
            }
            Console.ReadLine();
        }
        private static void PrintArray()
        {
            Console.WriteLine("doubleValuee:");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while(enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
               

            }

            Console.WriteLine("\n intValues :");
            enumerator = intValues.GetEnumerator();
            while(enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");

            }
            Console.WriteLine("\n intValuesCopy: ");
            foreach (var  element in intValuesCopy)
            {
                Console.WriteLine(element + " ");
            }
            Console.WriteLine();
        }
    }
}
