﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud
{
    class Program
    {

        static int employee_id = 0, department_id_ = 0, emp_dept_id;
       static  string name;
        static List<Employee> employees = new List<Employee>();
        static List<Department> departments = new List<Department>();
        static void Main(string[] args)
        {

            
            //create employee name
            while (true)
            {
                Console.WriteLine("enter choice:");

                Console.WriteLine(" 1.Create employee:\n");
                Console.WriteLine(" 2. Read specific employee:\n");
                Console.WriteLine(" 3. Read all employee:\n");
                Console.WriteLine(" 4. Update  employee:\n");
                Console.WriteLine(" 5. Delete employee:\n");

                Console.WriteLine(" 6. Create department:\n");
                Console.WriteLine(" 7. Read specific department:\n ");
                Console.WriteLine(" 8. Read all departments:\n");
                Console.WriteLine(" 9. Update  department:\n");
                Console.WriteLine(" 10. Delete  department:\n");

                Console.Write("------------------------------------------------");
                Console.Write("\n\n");


                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        
                            Console.WriteLine("Enter employee name: ");
                            name = Console.ReadLine();
                            Console.WriteLine("Enter dept id of employee: ");
                            emp_dept_id = Convert.ToInt32(Console.ReadLine());

                        create_employee(name,emp_dept_id);
                           
                        
                        break;
                    case 2:  //find emp deatils
                        Console.WriteLine("Find the emp id");
                        int find_e = Convert.ToInt32(Console.ReadLine());
                        find_employee(find_e);
                        break;
                    case 3:  //print all employees
                        foreach (var item in employees)
                        {
                            item.print();
                            Console.WriteLine("*********************");
                        }
                        break;
                    case 4:  //modify employee
                        Console.WriteLine("enter  the emp id to modify");
                        int update_e = Convert.ToInt32(Console.ReadLine());
                        modify_employee(update_e);
                        break;
                    case 5: //delete employee
                        Console.WriteLine("Find the emp id to be delletd");
                        int del_e = Convert.ToInt32(Console.ReadLine());
                        delete_emp(del_e);
                        break;
                    case 6:  //enter depatment details
                       
                            Console.WriteLine("enter department name");
                            string department_name = Console.ReadLine();
                            Console.WriteLine("enter deptt location");
                            string department_location = Console.ReadLine();
                            departments.Add(new Department((department_id_ + 1), department_name, department_location));
                            department_id_++;

                        
                        break;
                    case 7: //find dept details
                        Console.WriteLine("Find the dept id");
                        int find_d = Convert.ToInt32(Console.ReadLine());
                        find_dept(find_d);
                        break;
                    case 8:
                        foreach (var item in departments)
                        {
                            item.print_dept();
                            Console.WriteLine("********************************");
                        }
                        break;
                    case 9:
                        //modify dept
                        Console.WriteLine("enter  the dept id to modify");
                        int update_dept = Convert.ToInt32(Console.ReadLine());
                        modify_dept(update_dept);
                        break;
                    case 10: //delete department
                        Console.WriteLine("find the dept id to be delletd");
                        int del_d = Convert.ToInt32(Console.ReadLine());
                        delete_dept(del_d);
                        break;



                }
            }












            //display all employees
           
        }



        private static void create_employee(string name, int emp_dept_id)
        {
            int flag = 0;
            foreach (var item in departments)
            {
               
                if (item.d_id == emp_dept_id)
                {
                    employees.Add(new Employee((employee_id + 1), name, (emp_dept_id )));
                    employee_id++;
                    
                    flag = 1;
                }
            }
            if (flag == 0)
            {
                Console.WriteLine("\n First enter department\n");
                return;
            }
        }

        private static void find_employee(int find_key)
        {
            //int f = 0;
            //foreach (var item in employees)
            //{
            //    if (item.eid == find_key)
            //    {
            //        item.print();
            //        f = 1;
            //    }
            //}
            //if (f != 1)
            //{
            //    Console.WriteLine("not found ");
            //}
            var index = employees.Find(x=>x.eid==find_key);
            index.print();
        }








        private static void display_emp()
        {
            foreach (var item in employees)
            {
                Console.WriteLine("\n The employees are : ");
                item.print();
                Console.WriteLine("\n");

            }
        }
        private static void modify_employee(int para_mod_key)
        {
            Console.WriteLine("enter new name");
            string new_name = Console.ReadLine();
            Console.WriteLine("enter new dept id:");
            int new_dept_id = Convert.ToInt32(Console.ReadLine());
            foreach (var item in employees)
            {
                if (item.eid == para_mod_key)
                {
                    item.ename = new_name;
                    item.edept_id = new_dept_id;
                }
            }
        }
        private static void modify_dept(int para_dept_id)
        {
            Console.WriteLine("enter new name of the dept:");
            string new_name_dept = Console.ReadLine();
            Console.WriteLine("enter new dept location:");
            string new_loc = Console.ReadLine();

            foreach (var item in departments)
            {

                if (item.d_id == para_dept_id)
                {
                    item.d_location = new_loc;
                    item.d_name = new_name_dept;
                }

            }

        }
        private static void delete_dept(int para_del_key)
        {
            foreach (var emp in employees)
            {
                if (emp.edept_id == para_del_key)
                {
                    Console.WriteLine("Department cant be deleted...");
                    return;
                }
            }
            var itemto = departments.Single(r => r.d_id == para_del_key);
            departments.Remove(itemto);
        }


        private static void delete_emp(int para_del_emp)
        {
            //foreach (var emp in employees)
            //{
            //    if (emp.eid == para_del_emp)
            //    {
            //        Console.WriteLine("Deleting employee..\n ");
            //        return;
            //    }
            //}
            var itemto = employees.Single(r => r.eid == para_del_emp);
            employees.Remove(itemto);
        }


        private static void find_dept(int find_d)
        {


            int f1 = 0;
            foreach (var item in departments)
            {

                if (item.d_id == find_d)
                {
                    item.print_dept();
                    f1 = 1;
                }

            }
            if (f1 != 1)
            {
                Console.WriteLine("dept not found ");
            }



        }
    }
}



