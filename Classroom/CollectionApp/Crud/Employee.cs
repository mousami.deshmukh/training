﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud
{
    class Employee
    {
       public int edept_id;
       public string ename;
        public int eid;
        public Employee(int e_id,string e_name,int e_dept_id)
        {
            eid = e_id;
            edept_id = e_dept_id;
            ename = e_name;
        }
        public void print()
        {
            Console.WriteLine("ID: " + eid + "\n Name: " + ename + "\n Dept id of emp: " + edept_id);

        }
        
    }
}
