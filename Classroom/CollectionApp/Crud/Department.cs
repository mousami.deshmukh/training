﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crud
{
    class Department
    {
        public int d_id;
        public string d_name, d_location;
        public Department(int d_i,string d_n,string d_l)
        {
            d_id = d_i;
            d_name = d_n;
            d_location = d_l;
        }

        public void print_dept()
        {
            Console.WriteLine("ID :"+d_id+ "\n Name: " +d_name+ "\n Location: "+d_location);
        }
    }
}
