﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            bool aBoolean = true;
            char aChar = '$';
            int anInt = 34567;
            string aString = "hello";
            //use push() method
            stack.Push(aBoolean);
            PrintStack(stack);
            stack.Push(aChar);
            PrintStack(stack);
            stack.Push(anInt);
            PrintStack(stack);
            stack.Push(aString);
            PrintStack(stack);

            Console.WriteLine("first method in stack is {0} ", stack.Peek());
            PrintStack(stack);

            object o = stack.Pop();
            Console.WriteLine(o.GetType());
        }   

            private static void PrintStack(Stack stack)
            {
                if (stack.Count == 0)

                {
                    Console.WriteLine("Stack is empyt");
                }
                else
                {
                    Console.WriteLine("The stack is:");
                foreach (var item in stack)
                {
                    Console.WriteLine("{0} ",item);
                }
                }


            }
        
    }
}
