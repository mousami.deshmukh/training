﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sec_shoppingmall
{
    class Program
    {
        static int flag;
        static void Main(string[] args)
        {
            
                while (true)
                {
                try
                {
                    if (flag == 1)
                    {
                        Console.WriteLine("Enter choice");
                        Console.WriteLine("1. Display all Products\n");
                        Console.WriteLine("2. See catagory wise products\n ");
                        Console.WriteLine("3. Add product to cart\n");
                    }
                    else
                    {
                        Console.WriteLine("Enter choice");
                        Console.WriteLine("1. Display all Products\n");
                        Console.WriteLine("2. See catagory wise products\n ");
                        Console.WriteLine("3. Add product to cart\n");
                        Console.WriteLine("4. Display Cart details\n");
                        Console.WriteLine("5. Add qty of existing product in cart\n");
                        Console.WriteLine("6. Remove product from cart\n");
                        Console.WriteLine("7. See summary of cart\n");
                        Console.WriteLine("8.Place order\n");

                    }
                   
                    int ch = Convert.ToInt32(Console.ReadLine());

                    switch (ch)
                    {
                        case 1:
                            Console.WriteLine("The products are :    ");
                            Display_Prod();
                            ClearIt();

                            break;
                        case 2:
                            Console.WriteLine("The catagories are :");
                            Catagories();
                            Console.WriteLine("Enter the catagory");
                            string cata = Console.ReadLine();
                            Display_catagories(cata);


                            break;
                        case 3:
                            Console.WriteLine("The products are :    ");
                            Display_Prod();
                            Console.WriteLine("Enter the product name");
                            string p_n = Console.ReadLine();
                            Console.WriteLine("Enter the product qty");
                            int p_q = Convert.ToInt32(Console.ReadLine());
                            Insert_to_cart(p_n, p_q);
                            break;
                        case 4:
                            Console.WriteLine("The cart deatils are: ");
                            Display_Cart();
                            break;
                        case 5:
                            Console.WriteLine("Enter the product name whose qty is to be added:");
                            string p_n1 = Console.ReadLine();
                            Console.WriteLine("Enter the new product qty");
                            int p_q2 = Convert.ToInt32(Console.ReadLine());
                            Add_quantity(p_n1, p_q2);
                            break;
                        case 6:
                            Console.WriteLine("Enter the product name to be reomved from cart:");
                            string p_n2 = Console.ReadLine();
                            Remove_from_cart(p_n2);
                            break;
                        case 7:
                            Console.WriteLine("The summary of cart is ");
                            summary();
                            Sub();
                            break;
                        case 8:
                            Console.WriteLine("Order placed...");
                            Console.WriteLine("Deleting the cart...");
                            Delete_cart();
                            break;
                        default:
                            Console.WriteLine("Please enter correct choice");
                            break;



                    }
                }
                catch (SystemException)
                {
                    Console.WriteLine("Please enter correct format");
                }
            }
           

            // Display_Prod();
            // Insert("Cosco", 5);
            //add_qty("HT",2);
            //Display_Cart();
            //remove_fromcart("Cosco");
            //summary();
        }
        public static void ClearIt()
        {
            Console.WriteLine("Enter any key....");
            Console.ReadLine();
            Console.Clear();
        }
        public static void Delete_cart()
        {
            try
            {
                var db9 = new TrainingDBDataClasses1DataContext();
                var nums = from r in db9.Carts
                           select r;
                foreach (var item in nums)
                {
                    db9.Carts.DeleteOnSubmit(item);
                }
                flag = 1;
            }
            catch(Exception e)
            {
                Console.WriteLine("Already deleted");
            }

        }
        public static void Catagories()
        {
            var db8 = new TrainingDBDataClasses1DataContext();
            
            var cats = from t in db8.Products
                       select t;
            foreach (var item in cats)
            {
                Console.WriteLine("catagory is {0}", item.Catagory);
            }
        }
        
        public static void Display_catagories(string cat)
        {
            var db3 = new TrainingDBDataClasses1DataContext();
            
            var result2 = from a in db3.Products
                          where a.Catagory == cat
                          select a;

            foreach (var item in result2)
            {
                Console.WriteLine("Name: {0}\nDescription: {1}\nCatagory: {2}\nPrice {3}", item.Name, item.Description, item.Catagory, item.Price);
            }
        }
        public static void Display_Prod()
        {
            var db1 = new TrainingDBDataClasses1DataContext();
            var products = from c in db1.Products
                           select c;
            foreach (var item in products)
            {
                Console.WriteLine("Name: {0}\nDescription: {1}\nCatagory: {2}\nPrice {3}", item.Name, item.Description, item.Catagory, item.Price);
                Console.WriteLine("***************************************************************");
            }
        }
        public static void Display_Cart()
        {
            var db6 = new TrainingDBDataClasses1DataContext();
            var carts = from a in db6.Carts
                        select a;
            foreach (var item in carts)
            {
                Console.WriteLine("Name: {0}\nQuantity: {1}",item.Name,item.Quantity);
                Console.WriteLine("***************************************************************");
            }
        }
        public static void Insert_to_cart(string n,int qty)
        {
            try
            {
                var db2 = new TrainingDBDataClasses1DataContext();
                Cart ct = new Cart();
                ct.Name = n;
                ct.Quantity = qty;




                db2.Carts.InsertOnSubmit(ct);
                db2.SubmitChanges();
                Console.WriteLine("Inserted successfully...");
            }
            catch(Exception E)
            {
                Console.WriteLine("CANNOT BE INSERTED, ALREADY EXISTS");
            }
            
        }

        public static void Sub()
        {
            var db3 = new TrainingDBDataClasses1DataContext();
            var subtotals = (from p in db3.Products
                     join q in db3.Carts on p.Name equals q.Name
                     //where p.Name == "n"
                     select new
                     {
                         q.Name,
                         mul = p.Price * q.Quantity
                     }).ToList();

            foreach (var cust in subtotals)
            {
                Console.WriteLine("The subtotal of {0} is {1}",cust.Name,cust.mul);
               
            }
           
        }
        public static void Call_console()
        {
            Console.Clear();
        }

        public static void Add_quantity(string name_add, int qty)
        {
            var db5 = new TrainingDBDataClasses1DataContext();
            var carts =
        from d in db5.Carts
        where d.Name == name_add
        select d;

            foreach (var cust in carts)
                cust.Quantity = cust.Quantity + qty;
            db5.SubmitChanges();
        }

        public static void Remove_from_cart(string name1)
        {
            var db6 = new TrainingDBDataClasses1DataContext();
            var products_to_delete =
    from p in db6.Carts
    where p.Name == name1
    select p;

            db6.Carts.DeleteAllOnSubmit(products_to_delete);

            db6.SubmitChanges();
        }

        public static void summary()
        {
            var db7 = new TrainingDBDataClasses1DataContext();
                            
            var Count = (from f in db7.Carts

                         select f.Quantity).Count();



            Console.WriteLine(string.Format("Total number of goodies is : {0} ", Count));



            //Response.Write(string.Format("Addition of Salary is : {0} ", salarySum));
        }

    }
}
